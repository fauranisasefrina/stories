from django.test import TestCase, Client
from django.urls import resolve
from .views import *

class UnitTest(TestCase):
    #General Tests#
    def test_url_exist(self):
        response = Client().get('/books/')
        self.assertEqual(response.status_code,200)

    def test_function_exist(self):
        found = resolve('/books/')
        self.assertEqual(found.func,books)

    def test_using_templates(self):
        response = Client().get('/books/')
        self.assertTemplateUsed(response, 'books.html')

    def test_JSON_page_content(self):
        response = Client().get('/books/')
        html_response = response.content.decode('utf8')
        self.assertIsNotNone(html_response)
        self.assertIn('<h1 class="display-4" id="book">BOOK &#128214</h1>', html_response)

    #JSON'S Tests#
    def test_url_exist_dua(self):
        response = Client().get('/books/readJSON/<str:query>/')
        self.assertEqual(response.status_code,200)

    def test_function_exist(self):
        found = resolve('/books/readJSON/<str:query>/')
        self.assertEqual(found.func,readJSON)