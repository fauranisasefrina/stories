$(document).ready(function () {
    $('#buttonSubmit').click(function () {
    var query = $('#search_input').val();
        $.ajax({
            url: "/books/readJSON/" + query,
            success: function (result) {
                $('#table-books').empty();
                for (i=0; i< result.items.length; i++) {
                    $('<tr>').append(
                        $('<td>').append('<img style="max-height: 150px;max-width: 150px" src=' + result.items[i].volumeInfo.imageLinks.thumbnail + "'/>"),
                        $('<td>').text(result.items[i].volumeInfo.title),
                        $('<td>').text(result.items[i].volumeInfo.authors),
                        $('<td>').text(result.items[i].volumeInfo.publisher),
                        $('<td>').append('<a href="' + result.items[i].volumeInfo.canonicalVolumeLink + '" target="_blank">LINK</a>')
                    ).appendTo('#table-books');
                }
            }
        });
    });
    $('#search_input').val("example");
    $('#buttonSubmit').click();
});