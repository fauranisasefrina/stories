from django.shortcuts import render
from django.http import JsonResponse
import json
import requests

def books(request):
    return render(request, 'books.html')

def readJSON(request,query):
    if request.user.is_authenticated:
        googlebooks = 'https://www.googleapis.com/books/v1/volumes?q='
        url = googlebooks + query
        json_data = json.loads(requests.get(url).text)
        return JsonResponse(json_data)
    if not request.user.is_authenticated:
        return render(request, 'books.html')