from django.urls import path
from . import views

app_name = 'user'

urlpatterns = [
    path('login/',views.userLogin,name='login'),
    path('register/',views.userRegister,name='register'),
    path('logout/',views.userLogout,name='logout'),
]