from django.contrib import auth
from django.contrib.auth.models import User
from django.test import TestCase, Client
from django.urls import resolve
import time
from . import views

class UsersUnitTest(TestCase):
    #     UNIT TESTS FOR LOGIN PAGE    #
    def test_login_is_exist(self):
        response = Client().get('/user/login/')
        self.assertEqual(response.status_code, 200)

    def test_login_using_login_func(self):
        foundlogin = resolve('/user/login/')
        self.assertEqual(foundlogin.func, views.userLogin)

    def test_login_using_login_template(self):
        response = Client().get('/user/login/')
        self.assertTemplateUsed(response, 'login.html')

    #     UNIT TESTS FOR REGISTER PAGE    #
    def test_register_is_exist(self):
        response = Client().get('/user/register/')
        self.assertEqual(response.status_code, 200)

    def test_register_using_register_func(self):
        foundregister = resolve('/user/register/')
        self.assertEqual(foundregister.func, views.userRegister)

    def test_register_using_register_template(self):
        response = Client().get('/user/register/')
        self.assertTemplateUsed(response, 'register.html')

    #     UNIT TESTS FOR LOGOUT   #
    def test_logout_is_exist(self):
        response = Client().get('/user/logout/')
        self.assertEqual(response.status_code, 302) #Redirect

    def test_logout_using_logout_func(self):
        foundlogout = resolve('/user/logout/')
        self.assertEqual(foundlogout.func, views.userLogout)

    #     UNIT TESTS FOR REGISTER/LOGIN/LOGOUT   #
    def setUp(self) :
        self.client = Client()
        new_user = User.objects.create_user(username='cobauser', email='coba@gmail.com', password='coba123')

    def test_user_register(self):
        registeruser = self.client.post('/user/register/', {'username': 'cobauser','email':'coba@gmail.com','password':'coba123'})
        response = self.client.get('/')
        html_response = response.content.decode('utf8')
        self.assertEqual(response.status_code, 200)

    def test_user_can_login(self):
        response = self.client.post('/user/login/', {'username': 'cobauser', 'password': 'coba123'})
        user = auth.get_user(self.client)
        self.assertTrue(user.is_authenticated)
        text = 'WELCOME, cobauser!'
        html_response = response.content.decode('utf8')
        self.assertIn(html_response,text)

    def test_user_cannot_login(self):
        response = self.client.post('/user/login/', {'username': 'coba!!!', 'password': 'coba123'})
        user = auth.get_user(self.client)
        self.assertFalse(user.is_authenticated)
        text = 'WELCOME, cobauser!'
        html_response = response.content.decode('utf8')
        self.assertNotIn(html_response, text)

    def test_user_can_logout(self):
        response = self.client.get('/')
        html_response = response.content.decode('utf8')
        self.assertNotIn('LOGOUT', html_response)
        self.client.login(username='cobauser',password='coba123')
        response = self.client.get('/user/logout/')
        html_response = response.content.decode('utf8')
        self.assertNotIn('LOGOUT', html_response)
