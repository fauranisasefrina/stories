from django.test import TestCase, Client
from django.urls import resolve
from .views import *

class UnitTest(TestCase):
    def test_url_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_function_exist(self):
        found = resolve('/')
        self.assertEqual(found.func,home)

    def test_using_templates(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'home.html')