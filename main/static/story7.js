$(".up").click(function() {
    var currentElement = $(this).parent().parent().parent();
    var previousElement = $(".accor" + (parseInt(currentElement.css("order")) - 1));
  
    var currentPanel = $(".panel" + currentElement.css("order"));
    var previousPanel = $(".panel" + previousElement.css("order"));
  
    //nilai order setelah tukeran
    var currentTmp = (parseInt(currentElement.css("order"))) - 1; //tempat sekarang
    var previousTmp = (parseInt(previousElement.css("order"))) + 1; //bekas tempat sebelumnya
  
    if (currentElement.css("order") > 1) {
        //menukar order
        previousElement.css("order", previousTmp);
        currentElement.css("order", currentTmp);
  
        //mengubah attr class
        currentElement.attr("class", "accor"+ currentTmp + " " + String(currentElement.attr("class")).substring(7));
        previousElement.attr("class", "accor"+ previousTmp + " " + String(previousElement.attr("class")).substring(7));
  
        currentPanel.attr("class", "panel" + currentTmp + " " + String(currentPanel.attr("class")).substring(7));
        previousPanel.attr("class", "panel" + previousTmp + " " + String(previousPanel.attr("class")).substring(7));
    };
  
  })
  
  $(".down").click(function() {
    var currentElement = $(this).parent().parent().parent();
    var nextElement = $(".accor" + (parseInt(currentElement.css("order")) + 1));
  
    var currentPanel = $(".panel" + currentElement.css("order"));
    var nextPanel = $(".panel" + nextElement.css("order"));
  
    var currentTmp = (parseInt(currentElement.css("order"))) + 1;
    var nextTmp = (parseInt(nextElement.css("order"))) - 1;
  
    if (currentElement.css("order") < 4) {
        nextElement.css("order", nextTmp);
        currentElement.css("order", currentTmp);
  
        currentElement.attr("class", "accor"+currentTmp + " " + String(currentElement.attr("class")).substring(7));
        nextElement.attr("class", "accor"+ nextTmp + " " + String(nextElement.attr("class")).substring(7));
  
        currentPanel.attr("class", "panel" + currentTmp + " " + String(currentPanel.attr("class")).substring(7));
        nextPanel.attr("class", "panel" + nextTmp + " " + String(nextPanel.attr("class")).substring(7));
    };
    
  });
  
  $(".accordion").click(function(event) {
    if ($(event.target).is("button")) {
        event.preventDefault(); //mencegah up down
    } else {
        var panelTarget = $(".panel" + parseInt($(this).css("order")));
        if (panelTarget.attr("class").includes("active")) { //sudah aktif
            panelTarget.removeClass("active");
            panelTarget.parent().css("background-color", "#555");
        } else { //belum aktif
            panelTarget.addClass("active");
            panelTarget.parent().css("background-color", "black");
        };
    };
  });
  